package wit.tech.jsbfp_firestation;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    FragmentHome fm = new FragmentHome();
                    switchfragment(fm);
                    return true;
                case R.id.navigation_notifications:
                    FragmentProfile fp = new FragmentProfile();
                    switchfragment(fp);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentHome fm = new FragmentHome();
        switchfragment(fm);

        SharedPreferences pref = MainActivity.this.getSharedPreferences("Prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        String titlex = pref.getString("title", null);

        setTitle("JSBFP - " + titlex);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void switchfragment(Fragment fm){
        FragmentTransaction fmt = getSupportFragmentManager().beginTransaction();
        fmt.replace(R.id.fragment_frame, fm, "fm");
        fmt.commit();
    }

}
