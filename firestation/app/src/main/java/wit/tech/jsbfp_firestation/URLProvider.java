package wit.tech.jsbfp_firestation;

/**
 * Created by Project Nana on 1/14/2018.
 */

public class URLProvider {
    public static final String USERS = "http://www.jsbfp.org/api/users.php";
    public static final String ALERTS = "http://www.jsbfp.org/api/alertManager.php";
}
