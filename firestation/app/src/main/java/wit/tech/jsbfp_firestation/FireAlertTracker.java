package wit.tech.jsbfp_firestation;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Project Nana on 1/14/2018.
 */

public class FireAlertTracker extends Service implements LocationListener{

    private Context context;

    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;

    Location location;
    double latitude;
    double longitude;

    private static final long MIN_DISTANCE = 0;
    private static final long MIN_TIME_UPDATE = 500;

    protected LocationManager locationManager;

    private String fs_id;

    public FireAlertTracker(){

    }

    public FireAlertTracker(Context context, String fs_id){

        this.context = context;
        this.fs_id = fs_id;
    }

    public Location getLocation(){

        try{
            locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(locationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER);

                if(isGPSEnabled){
                    canGetLocation = true;
                    if(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED){
                        return null;
                    }
                    if (location == null){
                        locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER,MIN_TIME_UPDATE,MIN_DISTANCE,this);
                        if (locationManager != null){
                            location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
                            if(location != null){
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }else if(isNetworkEnabled){
                    if(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED){
                        return null;
                    }
                    if (location == null){
                        locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER,MIN_TIME_UPDATE,MIN_DISTANCE,this);
                        if (locationManager != null){
                            location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
                            if(location != null){
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }


        }catch (Exception e){
            e.printStackTrace();
        }

        return location;
    }

    public void stopUsingGPS(){
        if(locationManager != null){
            if(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED){
                return;
            }
            locationManager.removeUpdates(FireAlertTracker.this);
        }
    }

    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
        return latitude;
    }

    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
        return longitude;
    }

    public void broadCastGps(String panic_id){
        //Toast.makeText(context,"Latitude: " + latitude + " - Longitude: " + longitude, Toast.LENGTH_SHORT).show();
       /* Intent alert = new Intent(getApplicationContext(), AlertActivity.class);
        startActivity(alert);*/


    }

    public boolean CanGetLocation(){
        return this.canGetLocation;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        // RECEIVE RESPONSE
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final JSONObject jResponse = new JSONObject(response);
                    String message = jResponse.getString("status");

                    if(message.equals("success")){

                        String client_name = jResponse.getString("client_name");
                        String client_lat = jResponse.getString("client_lat");
                        String client_long = jResponse.getString("client_long");

                        // SEND TO ANOTHER ACTIVITY

                        Intent dialogIntent = new Intent(context, FireAlertActivity.class);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("client_name", client_name);
                        mBundle.putDouble("client_lat", Double.parseDouble(client_lat));
                        mBundle.putDouble("client_long", Double.parseDouble(client_long));
                        dialogIntent.putExtras(mBundle);
                        context.startActivity(dialogIntent);

                    }else {
                        //Toast.makeText(context, "" + jResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        Log.i("Panic_response","" + jResponse.getString("status") );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("request", "FscheckFireAlert");
        params.put("fs_id", fs_id);

        APIRequest requestAlert = new APIRequest(URLProvider.ALERTS,params, responseListener);
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(requestAlert);

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
