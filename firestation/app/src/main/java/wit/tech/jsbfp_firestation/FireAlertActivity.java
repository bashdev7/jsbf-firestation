package wit.tech.jsbfp_firestation;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FireAlertActivity extends AppCompatActivity implements OnMapReadyCallback {

    MediaPlayer mp;

    MapView mapView;
    GoogleMap map;

    Button respond_btn , cancel_btn;

    // NEEDED IN MAP

    String client_name;
    Double client_lat,client_long;

    String fs_id;

    private static final int LOCATION_REQUEST = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fire_alert);

        respond_btn = (Button) findViewById(R.id.response_btn);
        cancel_btn = (Button) findViewById(R.id.cancel_btn);

        mp = MediaPlayer.create(getApplicationContext(), R.raw.firealarm);
        mp.start();

        SharedPreferences pref = FireAlertActivity.this.getSharedPreferences("Prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        fs_id = pref.getString("user_id", null);

        Bundle b = new Bundle();
        b = getIntent().getExtras();
        client_name = b.getString("client_name");
        client_lat = b.getDouble("client_lat");
        client_long = b.getDouble("client_long");

        mapView = (MapView) findViewById(R.id.mapView2);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

    }

    public void SendResponse(View view){
        switch (view.getId()){
            case R.id.response_btn:

                if(respond_btn.getText().toString().equals("SEND RESPONSE")){
                    // RECEIVE RESPONSE
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                final JSONObject jResponse = new JSONObject(response);
                                String message = jResponse.getString("status");

                                if(message.equals("success")){
                                    Toast.makeText(FireAlertActivity.this, "Response sent", Toast.LENGTH_LONG).show();

                                    respond_btn.setText("Dissmiss");
                                    cancel_btn.setVisibility(View.INVISIBLE);
                                }else {
                                    //Toast.makeText(context, "" + jResponse.getString("message"), Toast.LENGTH_SHORT).show();
                                    Log.i("Panic_response","" + jResponse.getString("message") );
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    };

                    HashMap<String, String> params = new HashMap<>();
                    params.put("request", "fsRespond");
                    params.put("fs_id", fs_id);

                    APIRequest requestAlert = new APIRequest(URLProvider.ALERTS,params, responseListener);
                    RequestQueue queue = Volley.newRequestQueue(FireAlertActivity.this);
                    queue.add(requestAlert);
                }else{
                    finish();
                }

                break;

            case R.id.cancel_btn:
                finish();
                break;
        }
    }

    @Override
    protected void onPause() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(FireAlertActivity.this);

        map = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(FireAlertActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(FireAlertActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(FireAlertActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            return;
        }

        MarkerOptions orgOption = new MarkerOptions();
        orgOption.position(new LatLng(client_lat,client_long));
        orgOption.title(client_name);
        orgOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        Marker orgMarker = map.addMarker(orgOption);
        orgMarker.showInfoWindow();

        map.setMyLocationEnabled(true);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(10.764846,122.579964), 10));
    }
}
