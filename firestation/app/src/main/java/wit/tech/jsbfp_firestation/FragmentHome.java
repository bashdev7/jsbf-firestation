package wit.tech.jsbfp_firestation;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    GoogleMap map;
    View v;

    Context mContext;

    ArrayList<LatLng> location_points = new ArrayList<LatLng>();
    ArrayList<String> marker_names = new ArrayList<String>();

    private double mLat;
    private double mLong;

    LocationManager locationManager;
    LocationListener locationListener;

    private final int FINE_LOCATION_REQUEST_CODE = 101;
    private final int COARSE_LOCATION_REQUEST_CODE = 102;

    String current_latitude = "";
    String current_longitude = "";

    String fs_id;

    private static final int LOCATION_REQUEST = 500;


    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_fragment_home, container, false);
        mContext = getContext();

        SharedPreferences pref = mContext.getSharedPreferences("Prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        fs_id = pref.getString("user_id", null);

        Toast.makeText(mContext, "ID: " + fs_id, Toast.LENGTH_SHORT).show();

        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        getCurrentLocation();

        FireAlertTracker fs = new FireAlertTracker(mContext, fs_id);
        fs.getLocation();

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = (MapView) v.findViewById(R.id.mapView);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(mContext);

        map = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            return;
        }
        map.setMyLocationEnabled(true);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(10.764846,122.579964), 10));

        getDevices();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case LOCATION_REQUEST:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    map.setMyLocationEnabled(true);
                }
                break;
        }
    }

    public void reloadMap(String status){
        map.clear();
        for (int x = 0; x < location_points.size(); x++){

            MarkerOptions orgOption = new MarkerOptions();
            orgOption.position(location_points.get(x));
            orgOption.title(marker_names.get(x));
            if(status.equals("normal")){
                orgOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            }else{
                orgOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
            Marker orgMarker = map.addMarker(orgOption);
            orgMarker.showInfoWindow();

        }

    }

    public void getDevices(){

        // SEND REQUEST TO THE SERVER
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final JSONObject jResponse = new JSONObject(response);
                    String status = jResponse.getString("status");
                    JSONArray jsonArray = jResponse.getJSONArray("content");

                    if(status.equals("success")){
                        location_points.clear();
                        marker_names.clear();
                        for (int x = 0; x < jsonArray.length(); x++){
                            JSONObject contents = jsonArray.getJSONObject(x);
                            String latitude = contents.getString("latitude");
                            String longitude = contents.getString("longitude");
                            String owner = contents.getString("owner");
                            String stats = contents.getString("stats");

                            location_points.add(new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude)));
                            marker_names.add(owner);

                            reloadMap(stats);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("request", "getDevices");

        APIRequest requestPanic = new APIRequest(URLProvider.ALERTS,params , responseListener);
        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(requestPanic);
    }


    private void getCurrentLocation() {

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                mLat = location.getLatitude();
                mLong = location.getLongitude();
                current_latitude = Double.toString(mLat);
                current_longitude = Double.toString(mLong);

                current_longitude=current_longitude.trim();
                current_latitude=current_latitude.trim();

                getDevices();

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {
                Log.d("Message", "Provider enabled");
            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };
        String locationProvide = LocationManager.NETWORK_PROVIDER;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},COARSE_LOCATION_REQUEST_CODE);
                return;
            }


        }
        if ( ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},FINE_LOCATION_REQUEST_CODE);
                return;
            }

        }

        locationManager.requestLocationUpdates(locationProvide, 0, 0, locationListener);
        Location lastLocation=locationManager.getLastKnownLocation(locationProvide);


    }

}
