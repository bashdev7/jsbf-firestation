package wit.tech.jsbfpclient;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    FragmentMap fm = new FragmentMap();
                    switchfragment(fm);
                    return true;
                case R.id.navigation_notifications:
                    FragmentProfile fp = new FragmentProfile();
                    switchfragment(fp);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Default Fragment
        FragmentMap fm = new FragmentMap();
        switchfragment(fm);
    }

    public void switchfragment(Fragment fm){
        FragmentTransaction fmt = getSupportFragmentManager().beginTransaction();
        fmt.replace(R.id.fragment_frame, fm, "fm");
        fmt.commit();
    }

}
