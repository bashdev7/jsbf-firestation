package wit.tech.jsbfpclient;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Project Nana on 1/13/2018.
 */

public class APIRequest extends StringRequest{
    private Map<String, String> params;

    public APIRequest(String URL_REQUEST, HashMap<String, String> args, Response.Listener<String> listener)
    {
        super(Method.POST,URL_REQUEST,listener,null);
        params = new HashMap<>();
        for (String key:args.keySet()) {
            params.put(key, args.get(key));
        }

    }

    @Override
    public Map<String, String>getParams(){return params;}

}
