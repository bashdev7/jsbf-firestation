package wit.tech.jsbfpclient;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RespondingFs extends AppCompatActivity {

    ArrayList<String> list_of_fs = new ArrayList<String>();
    ListView fs_respo;
    String user_id;

    Button stop_fire;

    MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_responding_fs);

        SharedPreferences pref = RespondingFs.this.getSharedPreferences("Prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        String lat = pref.getString("latitude", null);
        String lng = pref.getString("longitude",  null);

        user_id = pref.getString("user_id", null);

        stop_fire = (Button) findViewById(R.id.stop_fire);
        stop_fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopFire();
            }
        });

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        fs_respo = (ListView) findViewById(R.id.fs_respo);

        checkIfResponding();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void checkIfResponding(){
        // SEND REQUEST TO THE SERVER
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final JSONObject jResponse = new JSONObject(response);
                    String status = jResponse.getString("status");
                    JSONArray jsonArray = jResponse.getJSONArray("content");

                    if(status.equals("success")){
                        for (int x = 0; x < jsonArray.length(); x++){
                            JSONObject contents = jsonArray.getJSONObject(x);
                            String title = contents.getString("title");
                            list_of_fs.add(title);
                        }
                    }

                    String[] stockArr = new String[list_of_fs.size()];
                    stockArr = list_of_fs.toArray(stockArr);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(RespondingFs.this,
                            android.R.layout.simple_list_item_1, stockArr);

                    fs_respo.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("request", "checkRespond");
        params.put("id", user_id);

        APIRequest requestPanic = new APIRequest(URLProvider.ALERTS,params , responseListener);
        RequestQueue queue = Volley.newRequestQueue(RespondingFs.this);
        queue.add(requestPanic);
    }

    public void stopFire(){
        dialog = new MaterialDialog.Builder(RespondingFs.this)
                .title("Loading")
                .content("Please wait...")
                .progress(true, 0)
                .show();
        // SEND REQUEST TO THE SERVER
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final JSONObject jResponse = new JSONObject(response);
                    String status = jResponse.getString("status");

                    if(status.equals("success")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(RespondingFs.this);
                        builder.setMessage("Fire alert dismissed")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, int which) {
                                        Intent i = new Intent(RespondingFs.this, ActivityLogin.class);
                                        startActivity(i);
                                    }
                                })
                                .create()
                                .show();
                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(RespondingFs.this);
                        builder.setMessage("Fire alert dismissed")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, int which) {
                                        Intent i = new Intent(RespondingFs.this, ActivityLogin.class);
                                        startActivity(i);
                                    }
                                })
                                .create()
                                .show();

                    }
                    dialog.dismiss();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("request", "fireStop");
        params.put("id", user_id);

        APIRequest requestPanic = new APIRequest(URLProvider.ALERTS,params , responseListener);
        RequestQueue queue = Volley.newRequestQueue(RespondingFs.this);
        queue.add(requestPanic);
    }
}
