package wit.tech.jsbfpclient;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMap extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    GoogleMap map;
    View v;

    Button respond_btn;

    LinearLayout alert_panel;

    String user_id;

    Context mContext;

    // REQUIREMENTS FOR MAP

    ArrayList<LatLng> location_points = new ArrayList<LatLng>();
    ArrayList<String> marker_names = new ArrayList<String>();

    LatLng latLng;

    private double mLat;
    private double mLong;

    LocationManager locationManager;
    LocationListener locationListener;

    private static final int LOCATION_REQUEST = 500;

    private final int FINE_LOCATION_REQUEST_CODE = 101;
    private final int COARSE_LOCATION_REQUEST_CODE = 102;

    String current_latitude = "";
    String current_longitude = "";


    public FragmentMap() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_fragment_map, container, false);

        mContext = getContext();

        SharedPreferences pref = mContext.getSharedPreferences("Prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        String lat = pref.getString("latitude", null);
        String lng = pref.getString("longitude",  null);

        alert_panel = (LinearLayout) v.findViewById(R.id.alert_panel);

        respond_btn = (Button) v.findViewById(R.id.respond_btn);

        user_id = pref.getString("user_id", null);

        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        getCurrentLocation();

        latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

        checkIfUserHasAlerts();

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = (MapView) v.findViewById(R.id.mapView);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(getActivity());

        map = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);

        MarkerOptions orgOption = new MarkerOptions();
        orgOption.position(latLng);
        orgOption.title("Your Device");
        orgOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        Marker orgMarker = map.addMarker(orgOption);
        orgMarker.showInfoWindow();

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void reloadMap(){
        map.clear();
        for (int x = 0; x < location_points.size(); x++){

            MarkerOptions orgOption = new MarkerOptions();
            orgOption.position(location_points.get(x));
            orgOption.title(marker_names.get(x));
            orgOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            Marker orgMarker = map.addMarker(orgOption);
            orgMarker.showInfoWindow();

            //Create the URL to get request from first marker to second marker
            String url = getRequestUrl(location_points.get(x), latLng);
            TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
            taskRequestDirections.execute(url);

        }

        MarkerOptions orgOption = new MarkerOptions();
        orgOption.position(latLng);
        orgOption.title("Your Device");
        orgOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        Marker orgMarker = map.addMarker(orgOption);
        orgMarker.showInfoWindow();
    }

    public void clearMap(){
        map.clear();
        MarkerOptions orgOption = new MarkerOptions();
        orgOption.position(latLng);
        orgOption.title("Your Device");
        orgOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        Marker orgMarker = map.addMarker(orgOption);
        orgMarker.showInfoWindow();
    }

    private void getCurrentLocation() {

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                mLat = location.getLatitude();
                mLong = location.getLongitude();
                current_latitude = Double.toString(mLat);
                current_longitude = Double.toString(mLong);

                current_longitude=current_longitude.trim();
                current_latitude=current_latitude.trim();

                checkIfUserHasAlerts();

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {
                Log.d("Message", "Provider enabled");
            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };
        String locationProvide = LocationManager.NETWORK_PROVIDER;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},COARSE_LOCATION_REQUEST_CODE);
                return;
            }


        }
        if ( ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},FINE_LOCATION_REQUEST_CODE);
                return;
            }

        }

        locationManager.requestLocationUpdates(locationProvide, 0, 0, locationListener);
        Location lastLocation=locationManager.getLastKnownLocation(locationProvide);


    }


    public void checkIfUserHasAlerts(){
        // SEND REQUEST TO THE SERVER
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final JSONObject jResponse = new JSONObject(response);
                    String status = jResponse.getString("status");

                    if(status.equals("success")){
                        alert_panel.setVisibility(View.VISIBLE);
                        checkIfResponding();
                    }else{
                        alert_panel.setVisibility(View.INVISIBLE);
                        respond_btn.setVisibility(View.INVISIBLE);
                        clearMap();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("request", "checkAlerts");
        params.put("id", user_id);

        APIRequest requestPanic = new APIRequest(URLProvider.ALERTS,params , responseListener);
        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(requestPanic);
    }

    public void checkIfResponding(){

        respond_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, RespondingFs.class);
                startActivity(i);
            }
        });

        // SEND REQUEST TO THE SERVER
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final JSONObject jResponse = new JSONObject(response);
                    String status = jResponse.getString("status");
                    JSONArray jsonArray = jResponse.getJSONArray("content");

                    if(status.equals("success")){
                        respond_btn.setText("RESPONDING: " + marker_names.size());
                        respond_btn.setVisibility(View.VISIBLE);
                        location_points.clear();
                        marker_names.clear();
                        for (int x = 0; x < jsonArray.length(); x++){
                            JSONObject contents = jsonArray.getJSONObject(x);
                            String latitude = contents.getString("latitude");
                            String longitude = contents.getString("longitude");
                            String title = contents.getString("title");

                            location_points.add(new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude)));
                            marker_names.add(title);


                            reloadMap();
                        }
                    }else{

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("request", "checkRespond");
        params.put("id", user_id);

        APIRequest requestPanic = new APIRequest(URLProvider.ALERTS,params , responseListener);
        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(requestPanic);
    }


    private String getRequestUrl(LatLng origin, LatLng dest) {
        //Value of origin
        String str_org = "origin=" + origin.latitude +","+origin.longitude;
        //Value of destination
        String str_dest = "destination=" + dest.latitude+","+dest.longitude;
        //Set value enable the sensor
        String sensor = "sensor=false";
        //Mode for find direction
        String mode = "mode=driving";
        //Build the full param
        String param = str_org +"&" + str_dest + "&" +sensor+"&" +mode;
        //Output format
        String output = "json";
        //Create url to request
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
        return url;
    }

    private String requestDirection(String reqUrl) throws IOException {
        String responseString = "";
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try{
            URL url = new URL(reqUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            //Get the response result
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuffer stringBuffer = new StringBuffer();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }

            responseString = stringBuffer.toString();
            bufferedReader.close();
            inputStreamReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            httpURLConnection.disconnect();
        }
        return responseString;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case LOCATION_REQUEST:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    map.setMyLocationEnabled(true);
                }
                break;
        }
    }

    public class TaskRequestDirections extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String responseString = "";
            try {
                responseString = requestDirection(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return  responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Parse json here
            FragmentMap.TaskParser taskParser = new FragmentMap.TaskParser();
            taskParser.execute(s);
        }
    }

    public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>> > {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject = null;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jsonObject = new JSONObject(strings[0]);
                DirectionsParser directionsParser = new DirectionsParser();
                routes = directionsParser.parse(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            //Get list route and display it into the map

            ArrayList points = null;

            PolylineOptions polylineOptions = null;

            for (List<HashMap<String, String>> path : lists) {
                points = new ArrayList();
                polylineOptions = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    double lat = Double.parseDouble(point.get("lat"));
                    double lon = Double.parseDouble(point.get("lon"));

                    points.add(new LatLng(lat,lon));
                }

                polylineOptions.addAll(points);
                polylineOptions.width(15);
                polylineOptions.color(Color.GREEN);
                polylineOptions.geodesic(true);
            }

            if (polylineOptions!=null) {
                map.addPolyline(polylineOptions);
            } else {
                Toast.makeText(mContext, "Direction not found!", Toast.LENGTH_SHORT).show();
            }

        }
    }

}
