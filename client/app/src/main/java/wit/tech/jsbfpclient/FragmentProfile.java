package wit.tech.jsbfpclient;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */

public class FragmentProfile extends Fragment {

    // SQLITE AUTO LOGIN MODE
    public SQLiteDatabase database = null;
    private String DB_NAME = "Auth";
    private String TABLE_NAME = "Users";

    MaterialDialog dialog;

    String client_id;

    Button btnLogout;

    View v;

    public FragmentProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        v = inflater.inflate(R.layout.fragment_fragment_profile, container, false);

        SharedPreferences pref = getContext().getSharedPreferences("Prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        btnLogout = (Button) v.findViewById(R.id.app_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });

        client_id = pref.getString("user_id", null);

        return v;
    }

    public void logout(){
        database = getActivity().openOrCreateDatabase(DB_NAME,
                getActivity() .MODE_PRIVATE, null);

        database.execSQL("DELETE FROM " + TABLE_NAME + " WHERE user_id = '"+ client_id +"'");


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("You have logged out.")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, int which) {
                        Intent i = new Intent(getContext(), ActivityLogin.class);
                        startActivity(i);
                    }
                })
                .create()
                .show();

    }

}
