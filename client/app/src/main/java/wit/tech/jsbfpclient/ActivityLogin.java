package wit.tech.jsbfpclient;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ActivityLogin extends AppCompatActivity {

    // SQLITE AUTO LOGIN MODE
    public SQLiteDatabase database = null;
    private String DB_NAME = "Auth";
    private String TABLE_NAME = "Users";

    EditText username, password;
    Button login;

    MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        // CREATE DATABASE IF NOT EXISTS
        try{
            database = ActivityLogin.this.openOrCreateDatabase(DB_NAME,this.MODE_PRIVATE,null);
            database.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                    "(id integer primary key,user_id integer, isLogged VARCHAR);");
        }catch (Exception e){
            e.printStackTrace();
        }

        // QUERY TO CHECK IF USER IS SESSIONED
        Cursor curs = database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE isLogged = 'Yes'", null);

        // CHECK IF THERE IS A SESSION ISLOGGED STORED
        if(curs.getCount() > 0){

            Intent police_dashboard = new Intent(ActivityLogin.this, MainActivity.class);
            startActivity(police_dashboard);

            editor.apply();
            finish();
        }else{
            requestLogin();
        }


    }

    public void requestLogin(){
        username = (EditText) findViewById(R.id.login_username);
        password = (EditText) findViewById(R.id.login_password);

        login = (Button) findViewById(R.id.login_button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("Prefs", MODE_PRIVATE);
                final SharedPreferences.Editor editor = pref.edit();
                dialog = new MaterialDialog.Builder(ActivityLogin.this)
                        .title("Loading")
                        .content("Please wait...")
                        .progress(true, 0)
                        .show();

                // RESPONSE
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("Resssss", response);
                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this);
                        try {
                            JSONObject jResponse = new JSONObject(response);
                            String status = jResponse.getString("status");


                            if(status.equals("success")){

                                String id = jResponse.getString("id");
                                String firstname = jResponse.getString("firstname");
                                String lastname = jResponse.getString("lastname");
                                String contact = jResponse.getString("contact");
                                String address = jResponse.getString("address");
                                String latitude = jResponse.getString("latitude");
                                String longitude = jResponse.getString("longitude");

                                Log.i("Lattitude", latitude);

                                editor.putString("latitude",latitude);
                                editor.putString("longitude",longitude);
                                editor.putString("contact",contact);
                                editor.putString("user_id", id);
                                editor.putString("isLogged", "Yes");



                                // START DB
                                try{
                                    database = ActivityLogin.this.openOrCreateDatabase(DB_NAME,
                                            ActivityLogin.this .MODE_PRIVATE, null);

                                    database.execSQL("INSERT INTO " + TABLE_NAME + " (id,user_id,isLogged) VALUES ('1','" + id + "','Yes');");

                                }catch (Exception e){
                                    Log.e("DB ERROR", "Error creating db");
                                }

                                builder.setMessage("Login Successfull!")
                                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // TODO Auto-generated method stub

                                                Intent i = new Intent(ActivityLogin.this, MainActivity.class);
                                                startActivity(i);


                                                editor.apply();
                                                finish();
                                            }
                                        })
                                        .create()
                                        .show();
                            }else{
                                builder.setMessage("Error occured. Please try again later.")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };

                // BODY
                HashMap<String, String> params = new HashMap<>();
                params.put("request", "app_login");
                params.put("username", username.getText().toString());
                params.put("password", password.getText().toString());

                // SEND REQUEST
                APIRequest requestLogin = new APIRequest(URLProvider.USERS,params, responseListener);
                RequestQueue queue = Volley.newRequestQueue(ActivityLogin.this);
                queue.add(requestLogin);

            }
        });
    }
}
